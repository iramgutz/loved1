define({ "api": [
  {
    "type": "get",
    "url": "/v1/users",
    "title": "1 Request all users",
    "version": "1.0.0",
    "name": "AllUsers",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "0",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "0.id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "0.created_at",
            "description": "<p>Created date.</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "0.updated_at",
            "description": "<p>Last modification date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 200 Example",
          "content": " HTTP/1.1 200 OK\n[\n    {\n        id: 1,\n        created_at: 2016-11-14 05:24:54,\n        updated_at: 2016-11-14 05:24:54\n    },\n    {\n        id: 2,\n        created_at: 2016-11-14 05:24:54,\n        updated_at: 2016-11-14 05:24:54\n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/vagrant/Code/loved1/app/Http/Controllers/Loved1/UserController.php",
    "groupTitle": "Users"
  },
  {
    "type": "delete",
    "url": "/v1/users/:id",
    "title": "5 Delete a specific  user",
    "version": "1.0.0",
    "name": "DeleteUser",
    "group": "Users",
    "parameter": {
      "fields": {
        "Url params": [
          {
            "group": "Url params",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success 204 Example",
          "content": "HTTP/1.1 204 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "EntityNotFound 404": [
          {
            "group": "EntityNotFound 404",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>The id of the user was not found.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EntityNotFound 404 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Entity not found\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/vagrant/Code/loved1/app/Http/Controllers/Loved1/UserController.php",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/v1/users/:id",
    "title": "2 Request a specific user",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "Users",
    "parameter": {
      "fields": {
        "Url params": [
          {
            "group": "Url params",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Created date.</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Last modification date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 200 Example",
          "content": " HTTP/1.1 200 OK\n{\n    id: 1,\n    created_at: 2016-11-14 05:24:54,\n    updated_at: 2016-11-14 05:24:54\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "EntityNotFound 404": [
          {
            "group": "EntityNotFound 404",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>The id of the user was not found.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EntityNotFound 404 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Entity not found\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/vagrant/Code/loved1/app/Http/Controllers/Loved1/UserController.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/v1/users",
    "title": "3 Store a  user",
    "version": "1.0.0",
    "name": "StoreUser",
    "group": "Users",
    "parameter": {
      "fields": {
        "FormData": [
          {
            "group": "FormData",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 201",
            "type": "DateTime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Created date.</p>"
          },
          {
            "group": "Success 201",
            "type": "DateTime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Last modification date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 201 Example",
          "content": " HTTP/1.1 201 OK\n{\n    id: 1,\n    created_at: 2016-11-14 05:24:54,\n    updated_at: 2016-11-14 05:24:54\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "ValidationErrors 400": [
          {
            "group": "ValidationErrors 400",
            "type": "array[]",
            "optional": false,
            "field": "name",
            "description": "<p>List of errors for name field.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.0",
            "description": "<p>First error for name.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.1",
            "description": "<p>Second error for name.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ValidationErrors 400 Example",
          "content": "  HTTP/1.1 400 Bad Request\n{\n    errors: {\n         name: The name field is required\n    }\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/vagrant/Code/loved1/app/Http/Controllers/Loved1/UserController.php",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/v1/users/:id",
    "title": "4 Update a specific  user",
    "version": "1.0.0",
    "name": "UpdateUser",
    "group": "Users",
    "parameter": {
      "fields": {
        "Url params": [
          {
            "group": "Url params",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User unique id.</p>"
          }
        ],
        "FormData": [
          {
            "group": "FormData",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Created date.</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Last modification date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success 200 Example",
          "content": " HTTP/1.1 200 OK\n{\n    id: 1,\n    created_at: 2016-11-14 05:24:54,\n    updated_at: 2016-11-14 05:24:54\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "EntityNotFound 404": [
          {
            "group": "EntityNotFound 404",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>The id of the user was not found.</p>"
          }
        ],
        "ValidationErrors 400": [
          {
            "group": "ValidationErrors 400",
            "type": "array[]",
            "optional": false,
            "field": "name",
            "description": "<p>List of errors for name field.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.0",
            "description": "<p>First error for name.</p>"
          },
          {
            "group": "ValidationErrors 400",
            "type": "string",
            "optional": false,
            "field": "name.1",
            "description": "<p>Second error for name.</p>"
          }
        ],
        "ServerError 500": [
          {
            "group": "ServerError 500",
            "type": "string",
            "optional": false,
            "field": "error",
            "description": "<p>Server error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EntityNotFound 404 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Entity not found\n}",
          "type": "json"
        },
        {
          "title": "ValidationErrors 400 Example",
          "content": "  HTTP/1.1 400 Bad Request\n{\n    errors: {\n         name: The name field is required\n    }\n}",
          "type": "json"
        },
        {
          "title": "ServerError 500 Example",
          "content": "  HTTP/1.1 404 Not Found\n{\n    error: Server error. Try again.\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/vagrant/Code/loved1/app/Http/Controllers/Loved1/UserController.php",
    "groupTitle": "Users"
  }
] });
