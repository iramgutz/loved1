# Loved1

## Installation

```
composer install
```

```
cp .env.example .env
```

Generate random key of 32 characters and paste it into `.env` file

```
APP_KEY={RANDOMKEYOF32CHARACTERS}
```

### Run Migrations

```
php artisan migrate
```

```
php artisan migrate --path=database/migrations/Loved1/users
```

```
php artisan migrate
```

### API Documentation

Docs into public/docs/Loved1 folder

