<?php

/*
 * This file is part of the iramgutierrez/generate-resource-api project.
 *
 * (c) Iram Gutiérrez <iramgutzglez@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Http\Controllers\Loved1;

use Illuminate\Http\Request;
use App\Http\Controllers\Loved1\BaseController;
use App\Loved1\Repositories\UserRepository as Repository;
use App\Loved1\Managers\UserManager as Manager;

class UserController extends BaseController
{
    public function __construct(Repository $Repository, Manager $Manager)
    {
        return parent::__construct($Repository , $Manager);
    }

    /**
     * @api {get} /v1/users 1 Request all users
     * @apiVersion 1.0.0
     * @apiName AllUsers
     * @apiGroup Users
     *
     * @apiSuccess {Object[]}  0 User Object.
     * @apiSuccess {Number} 0.id Id.
     * @apiSuccess {DateTime} 0.created_at  Created date.
     * @apiSuccess {DateTime} 0.updated_at  Last modification date.
     *
     * @apiSuccessExample Success 200 Example
     *  HTTP/1.1 200 OK
     * [
     *     {
     *         id: 1,
     *         created_at: 2016-11-14 05:24:54,
     *         updated_at: 2016-11-14 05:24:54
     *     },
     *     {
     *         id: 2,
     *         created_at: 2016-11-14 05:24:54,
     *         updated_at: 2016-11-14 05:24:54
     *     }
     * ]
     *
     * @apiError (ServerError 500) {string} error Server error.
     *
     * @apiErrorExample {json} ServerError 500 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Server error. Try again.
     * }
     */
    public function index(Request $Request)
    {
        return parent::index($Request);
    }

    /**
     * @api {post} /v1/users 3 Store a  user
     * @apiVersion 1.0.0
     * @apiName StoreUser
     * @apiGroup Users
     *
     * @apiParam (FormData) {String} name User name.
     *
     * @apiSuccess (Success 201) {Number} id Id.
     * @apiSuccess (Success 201)  {DateTime} created_at  Created date.
     * @apiSuccess (Success 201)  {DateTime} updated_at  Last modification date.
     *
     * @apiSuccessExample {json} Success 201 Example
     *  HTTP/1.1 201 OK
     * {
     *     id: 1,
     *     created_at: 2016-11-14 05:24:54,
     *     updated_at: 2016-11-14 05:24:54
     * }
     *
     * @apiError (ValidationErrors 400)  {array[]} name  List of errors for name field.
     * @apiError (ValidationErrors 400)  {string} name.0  First error for name.
     * @apiError (ValidationErrors 400)  {string} name.1  Second error for name.
     *
     * @apiErrorExample {json} ValidationErrors 400 Example
     *   HTTP/1.1 400 Bad Request
     * {
     *     errors: {
     *          name: The name field is required
     *     }
     * }
     *
     * @apiError (ServerError 500) {string} error Server error.
     *
     * @apiErrorExample {json} ServerError 500 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Server error. Try again.
     * }
     */
    public function store(Request $Request)
    {
        return parent::store($Request);
    }

    /**
     * @api {get} /v1/users/:id 2 Request a specific user
     * @apiVersion 1.0.0
     * @apiName GetUser
     * @apiGroup Users
     *
     * @apiParam (Url params) {Number} id User unique id.
     *
     * @apiSuccess {Number} id Id.
     * @apiSuccess {DateTime} created_at  Created date.
     * @apiSuccess {DateTime} updated_at  Last modification date.
     *
     * @apiSuccessExample {json} Success 200 Example
     *  HTTP/1.1 200 OK
     * {
     *     id: 1,
     *     created_at: 2016-11-14 05:24:54,
     *     updated_at: 2016-11-14 05:24:54
     * }
     *
     * @apiError (EntityNotFound 404) {string} error The id of the user was not found.
     *
     * @apiErrorExample {json} EntityNotFound 404 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Entity not found
     * }
     *
     * @apiError (ServerError 500) {string} error Server error.
     *
     * @apiErrorExample {json} ServerError 500 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Server error. Try again.
     * }
     */
    public function show(Request $Request, $id)
    {
        return parent::show($Request , $id);
    }

    /**
     * @api {put} /v1/users/:id 4 Update a specific  user
     * @apiVersion 1.0.0
     * @apiName UpdateUser
     * @apiGroup Users
     *
     * @apiParam (Url params)  {Number} id User unique id.
     * @apiParam (FormData) {String} name User name.
     *
     * @apiSuccess {Number} id Id.
     * @apiSuccess {DateTime} created_at  Created date.
     * @apiSuccess {DateTime} updated_at  Last modification date.
     *
     * @apiSuccessExample {json} Success 200 Example
     *  HTTP/1.1 200 OK
     * {
     *     id: 1,
     *     created_at: 2016-11-14 05:24:54,
     *     updated_at: 2016-11-14 05:24:54
     * }
     *
     * @apiError (EntityNotFound 404) {string} error The id of the user was not found.
     *
     * @apiErrorExample {json} EntityNotFound 404 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Entity not found
     * }
     *
     * @apiError (ValidationErrors 400)  {array[]} name  List of errors for name field.
     * @apiError (ValidationErrors 400)  {string} name.0  First error for name.
     * @apiError (ValidationErrors 400)  {string} name.1  Second error for name.
     *
     * @apiErrorExample {json} ValidationErrors 400 Example
     *   HTTP/1.1 400 Bad Request
     * {
     *     errors: {
     *          name: The name field is required
     *     }
     * }
     *
     * @apiError (ServerError 500) {string} error Server error.
     *
     * @apiErrorExample {json} ServerError 500 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Server error. Try again.
     * }
     */
    public function update(Request $Request, $id)
    {
        return parent::update($Request , $id);
    }

    /**
     * @api {delete} /v1/users/:id 5 Delete a specific  user
     * @apiVersion 1.0.0
     * @apiName DeleteUser
     * @apiGroup Users
     *
     * @apiParam (Url params)  {Number} id User unique id.
     *
     * @apiSuccessExample Success 204 Example
     *  HTTP/1.1 204 OK
     *
     * @apiError (EntityNotFound 404) {string} error The id of the user was not found.
     *
     * @apiErrorExample {json} EntityNotFound 404 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Entity not found
     * }
     *
     * @apiError (ServerError 500) {string} error Server error.
     *
     * @apiErrorExample {json} ServerError 500 Example
     *   HTTP/1.1 404 Not Found
     * {
     *     error: Server error. Try again.
     * }
     */
    public function destroy(Request $Request, $id)
    {
        return parent::destroy($Request , $id);
    }
}
