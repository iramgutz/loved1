<?php

/*
 * This file is part of the iramgutierrez/generate-resource-api project.
 *
 * (c) Iram Gutiérrez <iramgutzglez@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Loved1\Entities;

use Illuminate\Database\Eloquent\Collection;
use IramGutierrez\API\Entities\BaseEntity;

class UserEntity extends BaseEntity
{
    protected $table = 'users';

    protected $fillable = ['id' , 'name', 'lastname', 'email', 'password', 'profile_picture', 'welcome_email'];

    protected $hidden = [];

    protected $appends = [];
}
