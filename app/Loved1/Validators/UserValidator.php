<?php

/*
 * This file is part of the iramgutierrez/generate-resource-api project.
 *
 * (c) Iram Gutiérrez <iramgutzglez@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Loved1\Validators;

use IramGutierrez\API\Validators\BaseValidator;
use App\Loved1\Entities\UserEntity as Entity;

class UserValidator extends BaseValidator
{
    protected $rules = [
        'name' => 'required',
        'lastname' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|confirmed'
    ];

    public function __construct(Entity $Entity)
    {
        parent::__construct($Entity);
    }

    public function getUpdateRules()
    {
        $rules = $this->getRules();

        $rules['email'] .= ','.$this->entity->id;

        $rules['password'] = 'confirmed';

        return $rules;
    }
}
