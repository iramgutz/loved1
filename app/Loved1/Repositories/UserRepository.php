<?php

/*
 * This file is part of the iramgutierrez/generate-resource-api project.
 *
 * (c) Iram Gutiérrez <iramgutzglez@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Loved1\Repositories;

use IramGutierrez\API\Repositories\BaseRepository;
use App\Loved1\Entities\UserEntity as Entity;

class UserRepository extends BaseRepository
{
    public function __construct(Entity $Entity)
    {
        parent::__construct($Entity);
    }
}
