<?php

/*
 * This file is part of the iramgutierrez/generate-resource-api project.
 *
 * (c) Iram Gutiérrez <iramgutzglez@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Loved1\Managers;

use IramGutierrez\API\Managers\BaseManager;
use App\Loved1\Entities\UserEntity as Entity;
use App\Loved1\Validators\UserValidator as Validator;

class UserManager extends BaseManager
{
    public function __construct(Entity $Entity, Validator $Validator)
    {
        return parent::__construct($Entity , $Validator);
    }
}
